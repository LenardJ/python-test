# Insight REST API
Lunit INSIGHT API를 기준으로 API 구축

### 내용
- [작업내용](#작업내용)
- [사용모듈](#사용모듈)
- [데이터베이스](#데이터베이스)
	- [ChestXRay](#chestxray)
	- [ChestXRayModel](#chestxraymodel)
	- [ChestXRayPredict](#chestxraypredict)
	- [Mammography](#mammography)
	- [MammographyModel](#mammographymodel)
	- [MammographyPredict](#mammographypredict)
	- [User](#user)
	- [Token](#token)
- [API](#api)
	- [Basic API](#basic-api)
	- [API Example](#api-example)
- [API 서버 실행하기](#api-서버-실행하기)
	- [Dockerfile로 실행하기](#dockerfile로-실행하기)
	- [docker-compose로 실행하기](#docker-compose로-실행하기)
----
### 작업내용
 - chest x ray api
 	- Lunit INSIGHT API의 POST, GET, PUT API 개발
 - mammography api
 	- Lunit INSIGHT API의 POST, GET, PUT API 개발
 - User, Token api
 	- OAuth 인증 방식을 구현하지 못하여 간단하게 유저 생성과 토큰 발급을 위해 개발

----
### 사용모듈

| Package         | Version      | Description                       |
|:----------------|:-------------|:----------------------------------|
| `Django`        | 2.1.7        | Django framework                  |
| `django-mysql`  | 3.0.0.post1  | JSONField를 사용하기 위한 모듈         |
| `mysqlclient`   | 1.4.2.post1  | Django에서 mysql 사용을 위한 모듈      |


----
### 데이터베이스
*Class를 기준으로 작성하였으며, Field, Type, Description 외의 정보는 테이블 하단에 작성.*

##### ChestXRay
|  Field      |Type								|description   |
|:------------|:------------------|:-------------------|
| id					| IntegerField			| Primary key				 |
|	upload_id		| UUIDField   			|	파일 업로드 후 발급하는 upload_id값|
|	Path				| CharField   			|	파일이 저장된 경로			|
|	created_at	| DateTimeField   	|	생성된 시간			|
|	updated_at	| DateTimeField   	|	수정된 시간			|
- Unique Key : upload_id

##### ChestXRayPredict
|  Field      			|Type								| Description  |
|:------------------|:------------------|:-----------------|
| id								| IntegerField			| primary key				|
|	chest_x_ray				| ForeignKey				|	ChestXRay 참조		|
|	chest_x_ray_model	| ForeignKey				|	ChestXRayModel 참조		|
|	pos_map						| JSONField  				|	predict 결과	|
|	pos_prob					| FloatField				|	predict 결과			|
|	prediction_time		| FloatField				|	predict 결과					|
|	jpg								| CharField					|	predict 결과			|
|	created_at				| DateTimeField 		|	생성된 시간			|
|	updated_at				| DateTimeField     |	수정된 시간			|
- Unique Key : (chest_x_ray, chest_x_ray_model)

#### ChestXRayModel
|  Field      			|Type								| Description  |
|:------------------|:------------------|:-----------------|
| id								| IntegerField			| primary key			 |
|	tag								| CharField					|	alias							|
|	model_name				| CharField					|	모델명							|
|	created_at				| DateTimeField			|	생성된 시간					|
|	updated_at				| DateTimeField			|	수정된 시간					|
- Unique Key : tag, model_name

##### Mammography
|  Field      |Type								|description   |
|:------------|:------------------|:-------------------|
| id					| IntegerField			| Primary key				 |
|	upload_id		| UUIDField   			|	파일 업로드 후 발급하는 upload_id값|
|	Path				| CharField   			|	파일이 저장된 경로			|
|	created_at	| DateTimeField   	|	생성된 시간			|
|	updated_at	| DateTimeField   	|	수정된 시간			|
- Unique Key : upload_id

##### MammographyPredict
|  Field      					|Type								| Description  |
|:----------------------|:------------------|:-----------------|
| id										| IntegerField			| primary key				|
|	mammography_rcc				| ForeignKey				|	Mammography 참조		|
|	mammography_lcc				| ForeignKey				|	Mammography 참조		|
|	mammography_rmlo			| ForeignKey				|	Mammography 참조		|
|	mammography_lmlo			| ForeignKey				|	Mammography 참조		|
|	mammography_model			| ForeignKey				|	MammographyModel 참조		|
|	pos_map								| JSONField  				|	predict 결과	|
|	pos_prob							| FloatField				|	predict 결과			|
|	prediction_time				| FloatField				|	predict 결과					|
|	jpg										| CharField					|	predict 결과			|
|	created_at						| DateTimeField 		|	생성된 시간			|
|	updated_at						| DateTimeField     |	수정된 시간			|
- Unique Key : (mammography_rcc, mammography_rcc, mammography_rcc, mammography_rcc, mammography_model)

#### MammographyModel
|  Field      			|Type								| Description  |
|:------------------|:------------------|:-----------------|
| id								| IntegerField			| primary key			 |
|	tag								| CharField					|	alias							|
|	model_name				| CharField					|	모델명							|
|	created_at				| DateTimeField			|	생성된 시간					|
|	updated_at				| DateTimeField			|	수정된 시간					|
- Unique Key : tag, model_name

#### User
|  Field      			|Type								| Description  |
|:------------------|:------------------|:-----------------|
| id								| IntegerField			| primary key			 |
|	email							| CharField					|	email							|
|	password					| CharField					|	password					|
|	name							| CharField					|	name							|
|	created_at				| DateTimeField			|	생성된 시간					|
|	updated_at				| DateTimeField			|	수정된 시간					|
- Unique Key : email

#### Token
|  Field      | Type							| Description  |
|:------------|:------------------|:-----------------|
| id					| IntegerField			| primary key			 |
|	user				| OneToOneField			|	User 참조					|
|	token				| CharField					|	토큰							|
|	created_at	| DateTimeField|dd	|	생성된 시간				|
|	updated_at	| DateTimeField|dd	|	수정된 시간				|
- Unique Key : token


----
### API
#### Basic API
| Path                        | Method      |description                        |
|:----------------------------|:------------|:----------------------------------|
| ``                          | GET         |health check                       |
| `health/`                   | GET         |health check                       |
| `users/`                    | POST        |유저 생성                            |
| `tokens/users/{email}/`     | POST        |유저의 토큰 생성                       |
| `tokens/users/{email}`      | GET         |유저의 토큰 조회                       |
| `cxr/upload/`               | POST        |chest x ray dicom file 업로드        |
| `cxr/models/`               | GET         |chest x ray predict 모델 조회        |
| `cxr/models/{tag}/predict`  | POST        |chest x ray predict 요청           |
| `mmg/upload/`               | POST        |mammography dicom file 업로드       |
| `mmg/models/`               | GET         |mammography predict 모델 조회          |
| `mmg/models/{tag}/predict`  | POST        |mammography predict 요청             |

#### API Example
- `유저등록`
	- Method:	POST
	- Path	:	users/
	- Reqeust
		- body
			```code
			{
				email: 'test@test.com',
				password: 'test',
				name: 'testuser'
			}
			```
	- Response
		```code
		{
			"status": 200,
			"message": "ok",
		}
		```
- `토큰 발급`
	- Method:	POST
	- Path	:	tokens/users/{email}
	- Response
		```code
		{
			"token": "A4D3B46DBF04471EA52204424DB4C396"
		}
		```

- `토큰 확인`
	- Method:	GET
	- Path	:	tokens/users/{email}
	- Request
		- querystring
			- ?password=test&name=testuser
	- Response
		```code
		{
			"token": "A4D3B46DBF04471EA52204424DB4C396"
		}
		```

- `chest x ray 업로드`
	- Method:	POST
	- Path	:	cxr/upload/
	- Request
		- header
			- Content-Type	: application/dicom
			- Authorization	: Bearer A881BCC359124062842DB4BAAF3E0817
		- body
			- file
	- Response
		```code
		{
		  "upload_id": "36146bf5-c2cd-4eb1-95c0-3a119a32cd2a"
		}
		```
- `chest x ray 모델 조회`
	- Method:	GET
	- Path	:	cxr/models/
	- Reqeust
		- header
			- Authorization	: Bearer A881BCC359124062842DB4BAAF3E0817
	- Response
		```code
		[
		  {
		    "tag": "v1.1",
		    "model_name": "v1.1"
		  }
		]
		```
- `chest x ray predict`
	- Method:	POST
	- Path	:	cxr/models/{tag}/predict/
	- Reqeust
		- header
			- Content-Type	: application/json
			- Authorization	: Bearer A881BCC359124062842DB4BAAF3E0817
		- body
			```code
			{
				case: {
					frontal: '36146bf5-c2cd-4eb1-95c0-3a119a32cd2a'
				},
				requirements: ['pos_prob', 'pos_map', 'jpg', 'prediction_time']
			}
			```
	- Response
		```code
		{
			"chest_x_ray_id": 9,
		  "chest_x_ray_model_id": 1,
		  "pos_map": "[[0.04625883098624647, 0.045193624732978374], [0.04478279823458341, 0.044857089208533436], [0.04495163211247765, 0.04506246411056885]]",
		  "pos_prob": 0.925334,
		  "prediction_time": 2.063766,
		  "jpg": "data:image/jpg;base64,iVBORw0KGgoAAAANSU...."
		}
		```
- `mammography 업로드`
	- Method:	POST
	- Path	:	mmg/
	- Request
		- header
			- Content-Type	: application/dicom
			- Authorization	: Bearer A881BCC359124062842DB4BAAF3E0817
		- body
			- file
	- Response
		```code
		{
		  "upload_id": "36146bf5-c2cd-4eb1-95c0-3a119a32cd2a"
		}
		```
- `mammography predict`
	- Method:	POST
	- Path	:	users/
	- Reqeust
		- header
			- Content-Type	: application/json
			- Authorization	: Bearer A881BCC359124062842DB4BAAF3E0817
		- body
			```code
			{
				"case":{
					"rcc":"6f1d2ec8-51db-4dda-9e38-b7cdb0557955",
					"lcc":"4048448b-15f2-4aef-9a3e-8720c0c58e52",
					"rmlo":"c40faa2a-2139-4274-9073-41160e106a4f",
					"lmlo":"bd76c233-9073-4f36-9fd2-be45afb3113c"},
					"requirements":["pos_prob","pos_map","jpg"]
			}
			```
	- Response
		```code
		{
		  "pos_prob": "{\"l\": 0.001802, \"r\": 0.831307}",
		  "pos_map": "{\"rcc\": [[0.04625883098624647, 0.045193624732978374], [0.04478279823458341, 0.04506246411056885]], \"lcc\": [[0.01625883098624647, 0.015193624732978374], [0.01478279823458341, 0.01506246411056885]], \"rmlo\": [[5.883098624647e-05, 9.3624732978374e-05], [8.279823458341e-05, 6.246411056885e-05]], \"lmlo\": [[0.02225883098624647, 0.022293624732978374], [0.02228279823458341, 0.02226246411056885]]}",
		  "jpg": "data:image/jpg;base64,iVBORw0KGgoAAAANSU...."
		}
		```
- `mammography 모델 조회`
	- Method:	GET
	- Path	:	mmg/models/
	- Reqeust
		- header
			- Authorization	: Bearer A881BCC359124062842DB4BAAF3E0817
	- Response
		```code
		[
		  {
		    "tag": "v1.1",
		    "model_name": "v1.1"
		  }
		]
		```
----
### API 서버 실행하기
*프로젝트의 도커파일을 사용하여 장고 서버를 띄우기 전에 mysql 서버가 필요하다.*

#### Dockerfile로 실행하기
1. Dockerfile 빌드해서 이미지 생성하기
```bash
	# 도커파일 빌드
	docker build -t insight .

	# 이미지 생성 확인
	docker image ls
```
2. mysql 5.7 도커 이미지를 받는다.
```bash
	docker pull mysql:5.7
```
3. mysql 서버 실행
```bash
	docker run -p 3306:3306 --name db -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=Insight mysql:5.7 --character-set-server=utf8 --collation-server=utf8_unicode_ci
```
4. 1번에서 빌드한 도커 이미지 run
```bash
	docker run -p 8000:8000 --link db -e DJANGO_DB_HOST=db --rm insight

	# 도커 프로세스 확인
	docker ps
```
5. 4번에서 확인한 도커 프로세스에서 장고 프로젝트의 컨테이너로 접속
```bash
	docker exec -it 2e54b9312ebc bash
```
6. 컨테이너 접속 후 migrate 실행
```bash
	python3 manage.py makemigrations
	python3 manage.py migrate
```
7. api 호출..

#### docker-compose로 실행하기
1. 프로젝트 루트 폴더로 이동 (docker-comse.yml이 위치한 폴더)
```bash
	docker-compose up -d
```
2. api 호출..
